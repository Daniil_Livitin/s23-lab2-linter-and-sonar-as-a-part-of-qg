# Lab 2 -- Linter and SonarQube as a part of quality gates


[![pipeline status](https://gitlab.com/Daniil_Livitin/s23-lab2-linter-and-sonar-as-a-part-of-qg/badges/master/pipeline.svg)](https://gitlab.com/Daniil_Livitin/s23-lab2-linter-and-sonar-as-a-part-of-qg/-/commits/master)

1) Screenshots of the SonarQube results:
![img.png](img.png)
![img_1.png](img_1.png)

2) Sonar cloud was connected by adding env variables in repo settings and adding neccessary instructions to CI/CD.
``` 
sonar_cloud:
  stage: sonar_cloud
  dependencies:
    - build
    - linter
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  image: maven:3-openjdk-11 # Or newer
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - mvn verify sonar:sonar -Dsonar.projectKey=Daniil_Livitin_s23-lab2-linter-and-sonar-as-a-part-of-qg
  allow_failure: true
```


## Result in sonar cloud:
![img_2.png](img_2.png)
